module.exports = {
    url: 'http://todomvc.com/examples/vanillajs/',

    elements: {
        fakefield: '.maman',
        textField: 'input[class= "new-todo"]',
        todoList: '.todo-list'

    },

    commands: [{
        verifyTodoHomepage() {
            return this
                .assert.visible('@textField', 'Homepage was not loaded as expected.');
        },

        createTodo(todo_text) {
            return this
                .click('@textField')
                .setValue('@textField', [todo_text, this.api.Keys.ENTER])
            // .waitForElementVisible('@fakefield','1')
        },
        
        verifyTodoAdded(todo_text) {
            return this
                .assert.containsText('@todoList', todo_text,`Could not find "${todo_text}" as expected.`)
        }
    }],
};
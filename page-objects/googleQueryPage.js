module.exports = {
    url: 'https://www.google.com/advanced_search',

    elements: {
        searchField: '[name="as_q"]',
        languageDropDown: '#lr_button',
        languageMenu: '#lr_menu',
        regionDropDown: '#cr_button',
        desiredLanguage: '.goog-menuitem[value= "lang_it"]',
        submitButton: 'input[value="Advanced Search"]',

    },

    commands: [{
        searchQuery(value) {
            return this
                .setValue('@searchField', value);
        },

        setFilter(filter, value) {
            if (filter == 'language') {
                return this
                    .click('@languageDropDown')
                    .waitForElementVisible('@languageMenu')
                    .click(`.goog-menuitem[value= "${value}"]`);
            } else if (filter == 'region') {
                return this
                    .click('@regionDropDown')
                    .click(`.goog-menuitem[value= "${value}"]`);
            }
        },

        search() {
            return this
                .click('@submitButton');
        },
    }]
};
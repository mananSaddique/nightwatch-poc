module.exports = {
    url: 'https://www.amazon.co.uk',

    elements: {
        searchField: '#twotabsearchtextbox',
        submitButton: '.nav-input',
        resultHeader: '.sg-col-inner'

    },

    commands: [{
        searchQuery(item) {
            return this
                .click('@searchField','Clicking search field')
                .setValue('@searchField', item,)
        },

        seach() {
            return this
                .click('@submitButton')
        },

        verifyResult(item) {
            return this
                .waitForElementPresent('@resultHeader','Result Page loaded as expected')
                .assert.containsText('@resultHeader', item, 'Result page header shows searched item')
        }
    }]
};
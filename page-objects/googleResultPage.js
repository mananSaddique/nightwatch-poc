module.exports = {
    elements: {
        resultPage: '.mn-hd-txt'
    },
    commands: [{
        verifyResultPageVisible() {
            return this
                .assert.visible('@resultPage', 'Result Page loaded')
        },

        verifyRelevantResults() {
            return this
                .waitForElementPresent('@resultPage')
                .assert.containsText('@resultPage', 'Italian pages', 'Result page only showing Italian Pages')
        },

        verifyRegionResults() {
            return this
                .assert.urlContains("countryAI")
        }
    }]
};
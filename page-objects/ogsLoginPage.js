module.exports = {
    url: 'https://ogs-bo-aws-3pp-int-0.nyxaws.net/',

    elements: {
        usernameField: 'input[name= "email"]',
        passwordField: '#password',
        loginButton: 'button[type= "submit"]',
        operatorList: '#availableoperators'

    },

    commands: [{
        verifyOgsLoginPage() {
            return this
                .assert.visible('@loginButton', 'Homepage was not loaded as expected.');
        },

        login(username, password) {
            return this
                .setValue('@usernameField', username)
                .setValue('@passwordField', password)
                .click('@loginButton')
        },

        verifyLoggedin() {
            return this
                .waitForElementVisible('@operatorList')
        }
    }],
};
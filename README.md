# nightwatch

Nightwatch framework implementation to run tests written in both BDD and Mocha format.
Cucumber BDD implementation using Nightwatch JS. 
Contains sample test to test REST api using SuperTest with CHAI assertions


Running Tests:

 - Run all test under 'tests' folder with command -> "npm test"
 - Run all tests under 'features' folder with command -> "npm run e2e-test"

 Running specific tests:

 - Run specific test file -> "npm run test folder/fileName.js"
 - Run specific BDD file -> "npm run e2e-test folder/fileName.feature"

 Running specific tags:

 - Run selected tag with command -> "npm test -- --tag google"
 - Debug test with command -> "npm run debug -- --tag google"
 - Run feature files with tag -> "./node_modules/.bin/cucumber-js --tags "@tag_you_want_to_run" --require cucumber.conf.js --require step-definitions --format node_modules/cucumber-pretty"
                                        OR 
- Run command -> "npm run e2e-test -- --tags @tag_name" 

Report Generation:

To generate reports, first run a test using the command "npm run e2e-test" with or without a mention of a specific file. The command script contains flags for generating cucumber This will generate a file called "cucmber_report.json" under the "report/results" folder. This is report result in JSON format. 

- Run a test or tests using command -> "npm run e2e-test"
- Inorder to view HTML reports, run command -> " node reporting;/index.js" 

Running Tests in Safari

In order to run Tests in safar, following changes need to be made

- Delete delete the --w3c or --legacy option so that those options aren't pushed. See last comment-> https://github.com/nightwatchjs/nightwatch-docs/issues/94 
- Enable Webdriver support -> https://developer.apple.com/documentation/webkit/testing_with_webdriver_in_safari

Once above changes are made in the "nightwatch.conf.js" file make the following changes and run your tests

- Change server path to  "server_path": "/usr/bin/safaridriver" for safari 
- Change default > browserName to "safari".

Helping materials used:


Nightwatch setup tutorial -> https://www.youtube.com/watch?v=WpeLDfa0VSY
SuperTest -> https://github.com/visionmedia/supertest
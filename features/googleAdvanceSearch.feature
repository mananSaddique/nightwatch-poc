Feature: Advance Google Search

    Scenario Outline: Scenario Outline name: Doing an Advance Google search

        Given I open Google's advance search page
        When they perform a search with a set language
            | search_query   | filter   | language   |
            | <search_query> | <filter> | <language> |

        Then result generated are based on their search query and language

        Examples:
            | search_query      | filter   | language |
            | Elon Musk         | language | lang_it  |
            | Lord of the rings | language | lang_it  |


Feature: Amazon Item Search

    Scenario Outline: Search for an item on Amazon
        Given a user on the amazon homepage
        When they search for an item
            | item   |
            | <item> |

        Then all items matching the search items should be returned
            | item   |
            | <item> |

        Examples:
            | item       |
            | Air Hockey |
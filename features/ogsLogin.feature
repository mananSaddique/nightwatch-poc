@ogs
Feature: Login to OGS

    Scenario Outline: Verify login to OGS

        Given a user navigates to OGS site
        When they enter with correct credentials
            | username   | password   |
            | <username> | <password> |

        Then they should be able to login

        Examples:
            | username | password |
            | nyx      | nogs     |


module.exports = {
    before: function (browser) {
        console.log('Setting up...');
        browser();

    },
    after: function (browser) {
        console.log('Closing down...');
        browser();
    },
    waitForConditionTimeout: 10000,
};
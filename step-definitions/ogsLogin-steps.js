const {
    client
} = require('nightwatch-api');
const {
    Given,
    Then,
    When
} = require('cucumber');

const ogsLogin = client.page.ogsLoginPage();

Given('a user navigates to OGS site', function () {
    return ogsLogin
        .navigate()
        .verifyOgsLoginPage()
});

When('they enter with correct credentials', function (dataTable) {
    var data = dataTable.hashes()
    return ogsLogin
        .login(data[0].username, data[0].password)

});

Then('they should be able to login', function () {
    return ogsLogin
        .verifyLoggedin()
});
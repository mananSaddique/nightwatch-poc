const {
    client
} = require('nightwatch-api');
const {
    Given,
    Then,
    When
} = require('cucumber');

const amazonHomepage = client.page.amazonHomepage();

Given('a user on the amazon homepage', function () {
    return amazonHomepage
        .navigate()
});

When('they search for an item', function (dataTable) {
    var data = dataTable.hashes()
    return amazonHomepage
        .searchQuery(data[0].item)
        .seach()
});

Then('all items matching the search items should be returned', function (dataTable) {
    var data = dataTable.hashes()
    return amazonHomepage
        .verifyResult(data[0].item)
});
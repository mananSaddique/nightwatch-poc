const {
    client
} = require('nightwatch-api');
const {
    Given,
    Then,
    When
} = require('cucumber');

const queryPage = client.page.googleQueryPage();
const resultPage = client.page.googleResultPage();


Given(/^I open Google's advance search page$/, () => {
    return queryPage
        .navigate()
});

When(/^they perform a search with a set language$/, function (table) {
    const data = table.hashes()
    return queryPage
        .searchQuery(data[0].search_query)
        .setFilter(data[0].filter, data[0].language)
        .search()
});

Then(/^result generated are based on their search query and language$/, () => {
    return resultPage
        .verifyResultPageVisible()
        .verifyRelevantResults()
});
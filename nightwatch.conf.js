module.exports = {
  "src_folders": ["tests"],
  "output_folder": ["reporting"],
  "page_objects_path": ['page-objects'],
  "test_runner": "mocha",
  "globals_path": "globals",

  "webdriver": {
    "start_process": true,
    // "server_path": "node_modules/.bin/chromedriver",
    "server_path": "/usr/bin/safaridriver",
    "port": 9515
  },

  "test_settings": {
    "default": {
      "desiredCapabilities": {
        "browserName": "safari",
        javascriptEnabled: true,
        acceptSslCerts: true
      }
    }
  }
}
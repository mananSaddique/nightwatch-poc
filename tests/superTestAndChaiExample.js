const req = require('supertest')
const expect = require('chai').expect;


describe('Backend services test', () => {

  request = req('http://jsonplaceholder.typicode.com');

  it('GET content', () => {
    request
      .get('/posts/1')
      .then(response => {
        console.log(response.body.title)
        expect(response.body.userId).to.equal(1)
      })
  })

  it('POST content', () => {
    request
      .post('/posts')
      .send({
        title: 'Main',
        body: 'Man',
        userId: 87
      })
      .set('Content-Type', 'application/json; charset=UTF-8')
      .then(response => {
        expect(response.body.title).to.equal('Main')
      })
  })
})
describe('Carrying out google search under the advance search', () => {

    it('Do an Advance search in Google Search with language filter set Italian', (browser) => {
        const queryPage = browser.page.googleQueryPage();
        const resultPage = browser.page.googleResultPage();

        queryPage
            .navigate()
            .searchQuery('Elon Musk')
            .setFilter('language', 'lang_it')
            .search()

        resultPage
            .verifyResultPageVisible()
            .verifyRelevantResults()

        browser.saveScreenshot('tests_output/google.png')
    })


    it('Do an Advance search in Google Search with region filter set ', (browser) => {
        const queryPage = browser.page.googleQueryPage();
        const resultPage = browser.page.googleResultPage();

        queryPage
            .navigate()
            .searchQuery('Elon Musk')
            .setFilter('region', 'countryAI')
            .search()

        resultPage
            .verifyResultPageVisible()
            .verifyRegionResults()

        browser.saveScreenshot('tests_output/google.png')
    })

})